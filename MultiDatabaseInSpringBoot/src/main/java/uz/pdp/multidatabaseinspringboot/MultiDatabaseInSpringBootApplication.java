package uz.pdp.multidatabaseinspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiDatabaseInSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiDatabaseInSpringBootApplication.class, args);
    }

}
